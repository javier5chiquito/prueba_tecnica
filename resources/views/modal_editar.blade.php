                       {!! Form::model($tweet,["id"=>"fr_edtweet"])!!}
                        <div>
                         <div class="form-group">
                         <label class="sr-only" for="tweet">Editar</label>
                         <!--<textarea name="tweet" class="form-control" id="tweet" rows="3" placeholder="escribe tu nuevo tweet aqui!"></textarea>-->
                          {!! Form::textarea("tweet",$tweet->content,["class"=>"form-control","rows"=>'3',"id"=>"edit-tweet","placeholder"=>""]) !!}
                          </div>
                        </div>

                       <div class="btn-toolbar justify-content-between float-right">
                         <div class="btn-group">
                           <button type="button" id="actualizar" data-url="{{route('tweet.update',$tweet->id)}}" class="btn btn-primary">Actualizar</button>
                         </div>
                       </div>
