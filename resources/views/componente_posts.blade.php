          @foreach($tweets as $key => $tweet)
              <!---Post-->
                <div class="card border-dark mb-3">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                 <img class="rounded-circle" width="45" src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS0DkgNEyvsCd5nyfbH_1F925Z3aPhl7V65v6HljgITsTikyXPU&usqp=CAU" alt="">
                                </div>
                                <div class="ml-2">
                                  <div class="h5 m-0">{{$tweet->name}}:</div>
                                  <div class="h7 text-muted"><i class="fa fa-clock-o"></i> {{$tweet->created_at}} </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <div class="card-body" id="tweet_{{$tweet->id}}">
                      <div class="text-muted h7 mb-2"><!-- fecha publicacion se cambia-->
                      </div>
                       <p class="card-text" id="edit_{{$tweet->id}}">
                         <!-- contenido -->
                         {!!$tweet->content!!}
                        </p>
                    </div>
                    <div class="card-footer">
                   @if(Auth::user()->id == $tweet->id_user)
                   {{--para verificar si el usuario logeado es el mismo autor de se asi permite editar--}}
                    <div class="mostrar_user float-right">
                     <a href="#" class="card-link btn btn-info editar_post" data-url="{{route('tweet.edit',$tweet->id)}}" data-id="{{$tweet->id}}"> Editar </a>
                     <a href="#" class="card-link btn btn-danger eliminar_post" data-url="{{route('tweet.destroy',$tweet->id)}}" data-id="{{$tweet->id}}"> Eliminar </a>
                    </div>
                   @endif
                    <!-- colocar iconos de like comentario y compartir-->
                    </div>
                </div>
                <!-- Post /////-->
          @endforeach