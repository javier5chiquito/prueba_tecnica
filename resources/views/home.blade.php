@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-info mb-3">
                <div class="card-header">My-tweets</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <!--- comienzo de crear tweet-->
                                <div class="p-3 mb-2 bg-gradient-info text-white">
                                    <div class="card-body">
                                        {!! Form::model(Request::all(),["id"=>"fr_tweet"])!!}
                                        <div id="NuevoTweet">
                                            <div class="form-group">
                                                <label class="sr-only" for="tweet">Nuevo Tweet</label>
                                                <!--<textarea name="tweet" class="form-control" id="tweet" rows="3" placeholder="escribe tu nuevo tweet aqui!"></textarea>-->
                                                {!! Form::textarea("tweet",null,["class"=>"form-control","rows"=>'3',"id"=>"tweet", "placeholder"=>"escribe tu nuevo tweet aqui!"]) !!}
                                            </div>
                                        </div>
                                        <div class="btn-toolbar justify-content-between float-right">
                                            <div class="btn-group float-right">
                                                <button type="button" id="publicar" data-url= "{{route('tweet.store')}}" class="btn btn-primary">Publicar</button>
                                            </div>
                                        </div>
                                        {!! Form::close()!!}
                                    </div>
                                </div>
                                <!-- fin de la vista crear tweet /////-->
                                <br><br>
                                <div id="posts" data-url="{{route('tweet.index')}}">
                                </div>
                                <!-- para los post o tweets /////-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
</script>
@endsection