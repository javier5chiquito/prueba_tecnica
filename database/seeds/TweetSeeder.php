<?php

use Illuminate\Database\Seeder;

class TweetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      factory(Tweet::class, 10)->create();
    }

    public function DatabaseSeeder(){
      $this->call(TweetTableSeeder::class);
    }
}
