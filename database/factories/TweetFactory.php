<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Tweet;
use Faker\Generator as Faker;

$factory->define(Tweet::class, function (Faker $faker) {
  	return [
        'content' => $faker->sentence(6),
        'user_id' => factory(\App\User::class)->create()->id
    ];
});
