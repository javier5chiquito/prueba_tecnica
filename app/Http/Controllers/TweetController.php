<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use Illuminate\Support\Facades\Auth;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {

      $tweets = Tweet::Tweets();
      //$tweet = json_decode(Tweet::Tweets(),true);
      //dd($tweet);
      return response()->json(["tweets"=>$tweets,"view" => view("componente_posts")->with('tweets',$tweets->original)->render(),'status'=>200]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $tweet = new Tweet();
        $tweet->content = $request->tweet;
        $tweet->user_id = Auth::id();
        $tweet->save();
       
      return response()->json(['success'=>true,'status'=>200]);  
    }

    /* probando listar tweets
    public function ltweets(){
      $tweets = Tweet::Tweets();
      //$tweet = json_decode(Tweet::Tweets(),true);
      //dd($tweet);
      return response()->json(["tweets"=>$tweets,"view" => view("componente_posts")->with('tweets',$tweets->original)->render(),'status'=>200]);
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //para mostrar tweet

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tweet = Tweet::find($id);
       
       return response()->json(["view" =>view("modal_editar", compact('tweet'))->render(),'success'=>true,'status'=>200]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $tweet = Tweet::find($id);
        $tweet->content = $request->tweet;
        $tweet->save();
       
      return response()->json(['success'=>true,'status'=>200]);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tweet = Tweet::find($id);
        //$tweet->content = $request->tweet;
        //$tweet->user_id = 1;
        $tweet->delete();
       
      return response()->json(['success'=>true,'status'=>200]);  
    }
}