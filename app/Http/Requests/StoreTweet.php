<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTweet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|min:2|max:280',
        ];
    }
  
    public function messages()
    {
        return [
            'content.required' => 'El Tweet es obligatorio.',
            'content.min' => 'Debe contener mas de 2 caracteres.',
            'content.max' => 'No debe ser mayor a 280 caracteres',
        ];
    }
}
