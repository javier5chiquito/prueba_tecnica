<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tweet extends Model
{
    //

   protected $table = 'tweets';

    public function tweet_user(){
        
      return $this->hasOne(User::class);
  	}

  	public function scopeTweets($query)
    {
       //return all tweets
        $query = User::join('tweets','tweets.user_id','=','users.id')
        				->select(
        					'tweets.id',
        					'tweets.content',
        					'users.name',
                            'users.id as id_user',
    						'tweets.created_at')
        				->orderBy('tweets.created_at','desc')
        				->get();

        return response()->json($query,200);
    
    }
 
    public function scopeMyTweets($query)
    {    
      //Retunr Tweet of user logggin
        $query = User::join('tweets','tweets.user_id','=','users.id')
        				->select(
        					'tweets.id',
        					'tweets.content',
        					'users.name',
    						'tweets.created_at')
        				->where('users.id','=', Auth::id()) // Prueba
                        //->where('users.id','=',Auth::user()->id)
        				->orderBy('tweets.created_at','desc')
        				->get();

        return response()->json($query,200);
    }
     
     //para traducir la fecha de creacion
    public function getCreatedAtAttribute()
    {
      return date('d-m-Y H:i:s',strtotime($this->created_at));
    }

}
